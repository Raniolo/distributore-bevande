import java.util.Date;

public class Bibita {

	public String nome;
	public double costo;
	public Date Scadenza;

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setCosto(double costo) {
		this.costo=costo;
	}

	public double getCosto() {
		return costo;
	}

	public Date setScadenza() {
		Scadenza = new Date(); // aggiungi 3 mesi
		return Scadenza;
	}

}
